SRCS = main.latex
OBJS = $(SRCS:.latex=.pdf)
CC = cluttex
ifeq (, $(shell which uplatex))
	FLAGS = -e lualatex
else
	FLAGS = -e uplatex
endif

$(OBJS): $(SRCS)
	$(CC) $(FLAGS) $(SRCS)

all: clean $(OBJS) $(TARGET)

clean:
	rm -f $(OBJS)
